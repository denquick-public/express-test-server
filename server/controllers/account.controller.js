import User from '../models/user.model';
import { create } from './user.controller';

function login(req, res, next) {
  const { email, password } = req.body;
  User.login(email, password)
    .then((result) => { res.json(result); })
    .catch(e => next(e));
}

const join = create;

export default { login, join };
