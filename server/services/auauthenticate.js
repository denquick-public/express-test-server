import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import UserModel from '../models/user.model';

export default function authenticate(req) {
  const credentional = req.header('user');
  const { email, password } = JSON.parse(credentional);
  const err = new APIError('Need authenticated!', httpStatus.UNAUTHORIZED);

  return UserModel
    .authenticate(email, password, err);
}
