import mongoose from 'mongoose';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;


after((done) => {
  // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.close();
  done();
});

const user = {
  email: 'denquick@gmail.com',
  password: 'super password',
};

const userIncorrect = {
  email: 'den@gmail.com',
  password: 'super password',
};

function createUser(done) {
  request(app)
    .post('/api/users')
    .send(user)
    .expect(httpStatus.OK)
    .then((res) => {
      user._id = res.body._id;
      done();
    })
    .catch(done);
}

function deleteUser(done) {
  request(app)
    .delete(`/api/users/${user._id}`)
    .expect(httpStatus.OK)
    .then(() => {
      done();
    })
    .catch(done);
}

describe('## Checking account', () => {
  before(createUser);
  after(deleteUser);

  it('should get success logined', (done) => {
    const { email, password } = user;
    request(app)
      .post('/api/account/login')
      .send({ email, password })
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.have.property('_id');
        expect(res.body).to.have.property('email');
        done();
      })
      .catch(done);
  });

  it('should get unsuccess logined with incorrect user', (done) => {
    const { email, password } = userIncorrect;
    request(app)
      .post('/api/account/login')
      .send({ email, password })
      .expect(httpStatus.NOT_FOUND)
      .then(() => {
        done();
      })
      .catch(done);
  });
});

describe('## Login incorrect data', () => {
  const { email, password } = user;

  it('Send empty data', (done) => {
    request(app)
      .post('/api/account/login')
      .expect(httpStatus.BAD_REQUEST)
      .then(() => {
        done();
      })
      .catch(done);
  });

  it('without email param', (done) => {
    request(app)
      .post('/api/account/login')
      .send({ password })
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        const { message } = res.body;
        expect(message).to.equal('"email" is required');
        done();
      })
      .catch(done);
  });

  it('without password param', (done) => {
    request(app)
      .post('/api/account/login')
      .send({ email })
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        const { message } = res.body;
        expect(message).to.equal('"password" is required');
        done();
      })
      .catch(done);
  });
});
