import Joi from 'joi';

export const create = {
  body: {
    title: Joi.string().required(),
  },
};

export const update = {
  body: {
    title: Joi.string().required(),
  },
  params: {
    campaingId: Joi.string().hex().required()
  }
};
