import crypto from 'crypto';

export default function getSHA1ofJSON(input) {
  return crypto.createHash('sha1').update(JSON.stringify(input)).digest('hex');
}
