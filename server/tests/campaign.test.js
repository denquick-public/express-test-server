import mongoose from 'mongoose';
import request from 'supertest';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

after((done) => {
  // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.close();
  done();
});

let user = {
  email: 'denquick@gmail.com',
  password: 'super password',
};

function createUser(done) {
  request(app)
    .post('/api/users')
    .send(user)
    .expect(httpStatus.OK)
    .then((res) => {
      user = res.body;
      done();
    })
    .catch(done);
}

function deleteUser(done) {
  request(app)
    .delete(`/api/users/${user._id}`)
    .expect(httpStatus.OK)
    .then(() => {
      done();
    })
    .catch(done);
}

describe('## Positive campaing', () => {
  before(createUser);
  after(deleteUser);


  let campaing = {
    title: 'Some title campaing',
  };

  it('Create campaing', (done) => {
    const { email, password } = user;
    request(app)
      .post('/api/campaing')
      .set('user', JSON.stringify({ email, password }))
      .send(campaing)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.have.property('_id');
        expect(res.body).to.have.property('title');
        expect(res.body).to.have.property('userId');
        expect(res.body.userId).to.equal(user._id);
        campaing = res.body;
        done();
      })
      .catch(done);
  });

  it('Update title', (done) => {
    const { email, password } = user;
    const newTitle = 'Title was updated';
    campaing.title = newTitle;
    request(app)
      .put(`/api/campaing/${campaing._id}`)
      .set('user', JSON.stringify({ email, password }))
      .send(campaing)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.have.property('_id');
        expect(res.body).to.have.property('title');
        expect(res.body.title).to.equal(newTitle);
        done();
      })
      .catch(done);
  });

  it('Update other properties', (done) => {
    const { email, password } = user;
    const newTitle = 'Title was updated';
    campaing.title = newTitle;
    campaing.type = 'active';
    campaing.message = { value: '555' };
    campaing.design = { value: '777' };
    request(app)
      .put(`/api/campaing/${campaing._id}`)
      .set('user', JSON.stringify({ email, password }))
      .send(campaing)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.have.property('_id');
        expect(res.body).to.have.property('title');
        expect(res.body).to.have.property('type');
        expect(res.body.message).to.have.property('value');
        expect(res.body.design).to.have.property('value');
        expect(res.body.title).to.equal(newTitle);
        done();
      })
      .catch(done);
  });

  it('Get campaing', (done) => {
    const { email, password } = user;
    request(app)
      .get(`/api/campaing/${campaing._id}`)
      .set('user', JSON.stringify({ email, password }))
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body._id).to.equal(campaing._id);
        done();
      })
      .catch(done);
  });

  it('Get list campaings', (done) => {
    const { email, password } = user;
    request(app)
      .get('/api/campaing')
      .set('user', JSON.stringify({ email, password }))
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.be.an('array');
        done();
      })
      .catch(done);
  });

  it('Get list campaing (with limit and skip)', (done) => {
    const { email, password } = user;
    request(app)
      .get('/api/campaing')
      .set('user', JSON.stringify({ email, password }))
      .query({ limit: 10, skip: 1 })
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.be.an('array');
        done();
      })
      .catch(done);
  });

  it('Get list campaings with params', (done) => {
    const { email, password, _id } = user;
    const params = {
      userId: _id
    };
    request(app)
      .get('/api/campaing/')
      .set('user', JSON.stringify({ email, password }))
      .send({ params })
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.be.an('array');
        expect(res.body.length).to.least(1);
        done();
      })
      .catch(done);
  });

  it('Delete campaing', (done) => {
    const { email, password } = user;
    request(app)
      .delete(`/api/campaing/${campaing._id}`)
      .set('user', JSON.stringify({ email, password }))
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.have.property('_id');
        done();
      })
      .catch(done);
  });
});
