import express from 'express';
import accountController from '../controllers/account.controller';
import validate from 'express-validation';
import { login, join } from '../validations/account';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/login')
  .post(validate(login), accountController.login);

router.route('/join')
  .post(validate(join), accountController.join);

export default router;
