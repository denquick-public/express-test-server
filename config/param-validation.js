import Joi from 'joi';

// POST /api/users
const createUser = {
  body: {
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  },
};

export default {

  createUser,

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      email: Joi.string().required(),
    },
    params: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required()
    }
  }
};
