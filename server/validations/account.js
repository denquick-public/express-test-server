import Joi from 'joi';

const validationSchema = {
  body: {
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  },
};

export const join = validationSchema;

export const login = validationSchema;
