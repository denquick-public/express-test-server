import mongoose from 'mongoose';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

after((done) => {
  // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.close();
  done();
});

const user = {
  email: 'denquick@gmail.com',
  password: 'super password',
};

function deleteUser(done) {
  if ('_id' in user) {
    request(app)
      .delete(`/api/users/${user._id}`)
      .then(() => {
        delete user._id;
        done();
      })
      .catch(done);
  }
}

describe('## Checking joun user', () => {
  after(deleteUser);

  it('should get success joined', (done) => {
    const { email, password } = user;
    request(app)
      .post('/api/account/join')
      .send({ email, password })
      .expect(httpStatus.OK)
      .then((res) => {
        user._id = res.body._id;
        expect(res.body).to.have.property('_id');
        expect(res.body).to.have.property('email');
        done();
      })
      .catch(done);
  });
});

describe('## Checking joun incorrect', () => {
  const { email, password } = user;

  it('Without params', (done) => {
    request(app)
      .post('/api/account/join')
      // .send({ email })
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        const { message } = res.body;
        expect(message).to.equal('"email" is required and "password" is required');
        done();
      })
      .catch(done);
  });

  it('without email param', (done) => {
    request(app)
      .post('/api/account/join')
      .send({ password })
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        const { message } = res.body;
        expect(message).to.equal('"email" is required');
        done();
      })
      .catch(done);
  });

  it('without password param', (done) => {
    request(app)
      .post('/api/account/join')
      .send({ email })
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        const { message } = res.body;
        expect(message).to.equal('"password" is required');
        done();
      })
      .catch(done);
  });

  it('Email incorrect', (done) => {
    request(app)
      .post('/api/account/join')
      .send({ email: 'some text', password })
      .expect(httpStatus.BAD_REQUEST)
      .then((res) => {
        const { message } = res.body;
        expect(message).to.equal('"email" must be a valid email');
        done();
      })
      .catch(done);
  });
});
