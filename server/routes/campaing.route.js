import express from 'express';
import controller from '../controllers/campaing.controller';
import validate from 'express-validation';
import { create, update } from '../validations/campaing';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(controller.list)
  .post(validate(create), controller.create);

router.route('/:campaingId')
  .get(controller.get)
  .put(validate(update), controller.update)
  .delete(controller.remove);

router.param('campaingId', controller.load);

export default router;
