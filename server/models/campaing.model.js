import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * User Schema
 */
const CampaingSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    default: 'default',
  },
  message: {
    type: Object,
  },
  design: {
    type: Object,
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
CampaingSchema.method({
});

/**
 * Statics
 */
CampaingSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((campaing) => {
        if (campaing) {
          return campaing;
        }
        const err = new APIError('No such campaing exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({ skip = 0, limit = 50 } = {}, params = {}) {
    const condition = this.find();
    if ('userId' in params) {
      condition.where('userId').equals(params.userId);
    }
    if ('type' in params && params.type !== 'all') {
      condition.where('type').equals(params.type);
    }
    return condition
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef Campaing
 */
export default mongoose.model('Campaing', CampaingSchema);
