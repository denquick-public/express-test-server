import CampaingModel from '../models/campaing.model';
import authenticate from '../services/auauthenticate';

function process(meta, call) {
  const { req, res, next } = meta;
  return authenticate(req)
    .then(call)
    .then(savedModel => res.json(savedModel))
    .catch(e => next(e));
}

function load(req, res, next, id) {
  CampaingModel.get(id)
    .then((model) => {
      req.campaing = model; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

function get(req, res) {
  authenticate(req)
    .then(() => {
      res.json(req.campaing);
    });
}

function list(req, res, next) {
  process({ req, res, next }, () => {
    const { limit = 50, skip = 0 } = req.query;
    return CampaingModel.list({ limit, skip }, req.body.params || req.query.params);
  });
}

function create(req, res, next) {
  process({ req, res, next }, (user) => {
    const userId = user._id;
    const { title } = req.body;
    const model = new CampaingModel({
      title,
      userId,
    });

    return model.save();
  });
}

function update(req, res, next) {
  process({ req, res, next }, () => {
    const campaing = req.campaing;
    Object.assign(campaing, req.body);

    return campaing.save();
  });
}

function remove(req, res, next) {
  process({ req, res, next }, () => {
    const model = req.campaing;
    return model.remove();
  });
}

export default { create, update, load, remove, get, list };
